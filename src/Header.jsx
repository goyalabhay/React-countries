import React from "react";

const Header = ({ isDark, setIsDark }) => {
  function handleMode() {
    document.body.classList.toggle("darkBackground");
    setIsDark(!isDark);
  }

  return (
    <div className={isDark ? "dark-mode header" : "header"}>
      <div className={isDark ? "dark-mode headerText" : " headerText"}>
        Where in the World?
      </div>
      <div
        onClick={() => handleMode()}
        className={isDark ? "dark-mode darkModeInfo" : "darkModeInfo"}
      >
        <div>
          <img className="darkModeIcon" src="/DarkMode.svg" />
        </div>
        <div className="darkModeText">Dark Mode</div>
      </div>
    </div>
  );
};

export default Header;
