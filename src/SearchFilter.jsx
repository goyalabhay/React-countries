const SearchFilter = ({ isDark, callApi, setApiData, apiData }) => {
  function handleSearchbar(e) {
    let target = e.target.value.toLowerCase();

    if (target === "") {
      callApi("https://restcountries.com/v3.1/all");
    }

    let filteredData = apiData.reduce((arr, data) => {
      if (data.name.common.toLowerCase().includes(target)) {
        arr.push(data);
      }
      return arr;
    }, []);

    setApiData(filteredData);
  }

  function handleDelete(e) {
    if (e.key === "Backspace") {
      callApi("https://restcountries.com/v3.1/all");
      return;
    }
  }

  function handleSelect(e) {
    let target = e.target.value;
    callApi(`https://restcountries.com/v3.1/region/${target}`);
  }

  return (
    <div className={isDark ? "darkBackground SearchFilter" : "SearchFilter"}>
      <div className={isDark ? "dark-mode Search" : "Search"}>
        <img className="searchImage" src="/Search.svg" alt="" />
        <input
          onKeyDown={(e) => handleDelete(e)}
          onChange={(e) => handleSearchbar(e)}
          className={isDark ? "dark-mode searchBox" : "searchBox"}
          type="text"
          placeholder="Search for a country..."
        />
      </div>
      <div className={isDark ? "dark-mode Filter" : "Filter"}>
        <select
          className={isDark ? "dark-mode dropDownList" : "dropDownList"}
          onChange={(e) => handleSelect(e)}
        >
          <option value="" disabled selected hidden>
            Filter by region
          </option>
          <option className={isDark ? "dark-mode" : ""} value="Africa">
            Africa
          </option>
          <option className={isDark ? "dark-mode" : ""} value="America">
            America
          </option>
          <option className={isDark ? "dark-mode" : ""} value="Asia">
            Asia
          </option>
          <option className={isDark ? "dark-mode" : ""} value="Europe">
            Europe
          </option>
          <option className={isDark ? "dark-mode" : ""} value="Oceania">
            Oceania
          </option>
        </select>
      </div>
    </div>
  );
};

export default SearchFilter;
