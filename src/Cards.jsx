import { Link, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";

export default function Card({ isDark }) {
  const [countryCode, setCountryCode] = useState("");
  const [flagData, setFlagData] = useState([]);
  const code = useParams();
  useEffect(() => {
    callApi(`https://restcountries.com/v3.1/alpha/${code.id}`);
        
  }, [countryCode]);

  async function callApi(url) {
    try {

      const res = await axios.get(url);
      setFlagData(res.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  return (
    <div className={isDark ? "darkBackground" : ""}>
      {flagData.map((item, index) => (
        <div
          id={index}
          className={isDark ? "darkBackground singleFlag" : "singleFlag"}
        >
          <Link to={"/"}>
            <button
              // onClick={handelBackbtn}
              className={isDark ? "darkBtn backBtn" : "backBtn"}
            >
              <img className="backImg" src="/Back.svg" alt="" /> Back
            </button>
          </Link>
          <div className={isDark ? "darkBackground flagData" : "flagData"}>
            <img
              src={item.flags.png}
              className={isDark ? "dark-mode flagImg" : "flagImg"}
            />
            <div className={isDark ? "dark flagDetails" : "flagDetails"}>
              <h1 className={isDark ? "dark-text" : ""}>{item.name.common}</h1>
              <div className=" flagDetailsData">
                <div className="flagDetailsData-1">
                  <h4 className={isDark ? "dark-text" : ""}>
                    <span> Native Name: </span>{" "}
                    {Object.entries(item.name.nativeName)[0][1].common}
                  </h4>
                  <h4 className={isDark ? "dark-text" : ""}>
                    <span> Population: </span> {item.population}
                  </h4>
                  <h4 className={isDark ? "dark-text" : ""}>
                    <span> Region: </span> {item.region}
                  </h4>
                  <h4 className={isDark ? "dark-text" : ""}>
                    <span> Sub Region: </span> {item.subregion}
                  </h4>
                  <h4 className={isDark ? "dark-text" : ""}>
                    <span> Capital: </span> {item.capital}
                  </h4>
                </div>
                <div className="flagDetailsData-2">
                  <h4 className={isDark ? "dark-text" : ""}>
                    <span> Top Level Domain: </span> {item.tld[0]}
                  </h4>
                  <h4 className={isDark ? "dark-text" : ""}>
                    <span> Currencies: </span>{" "}
                    {item.currencies ? (
                      Object.entries(item.currencies)[0][1].symbol
                    ) : (
                      <p></p>
                    )}
                  </h4>
                  <h4 className={isDark ? "dark-text" : ""}>
                    <span> Languages: </span>{" "}
                    {Object.entries(item.languages)[0][1]}
                  </h4>
                </div>
              </div>
              <div className="borderCountries">
                <h4 className={isDark ? "dark-text" : ""}>Border Countries:</h4>
                {item.borders ? (
                  item.borders.map((country, index) => {
                    return (
                      <Link to={`/flag/${country}`} key={index} onClick={()=> setCountryCode(country)}>
                      {console.log(country)}

                        <button 
                          
                          className={isDark ? "dark-mode-border" : ""}
                        >{`${country}`}</button>
                      </Link>
                    );
                  })
                ) : (
                  <p></p>
                )}
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}
