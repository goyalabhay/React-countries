import React from "react";
import { Link } from "react-router-dom";

const AllCards = ({ apiData, isDark }) => {
  return (
    <div className={isDark ? "darkBackground" : ""}>
      <div className="allCards">
        {apiData.map((country, index) => (
          <Link
            to={`/flag/${country.ccn3}`}
            key={index}
            className={isDark ? "dark-mode card" : "card"}
          >
            <img src={country.flags.png} alt="" />
            <div className="countryInfo">
              <div className={isDark ? "dark-text countryName" : "countryName"}>
                {country.name.common}
              </div>
              <div className={isDark ? "dark-text" : ""}>
                <span
                  className={
                    isDark ? "dark-text countryPopulation" : "countryPopulation"
                  }
                >
                  Population:{" "}
                </span>
                {country.population}
              </div>
              <div
                className={isDark ? "dark-text countryRegion" : "countryRegion"}
              >
                <span className={isDark ? "dark-text" : ""}>Region: </span>
                {country.region}
              </div>
              <div
                className={
                  isDark ? "dark-text countryCapital" : "countryCapital"
                }
              >
                <span className={isDark ? "dark-text" : ""}>Capital: </span>
                {country.capital}
              </div>
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default AllCards;
