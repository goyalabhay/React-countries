import React, { useEffect, useState } from "react";
import axios from "axios";
import Header from "./Header";
import SearchFilter from "./SearchFilter";
import AllCards from "./AllCards";
import Cards from "./Cards";
import { Route, Routes } from "react-router-dom";

const App = () => {
  const api = "https://restcountries.com/v3.1/all";

  const [apiData, setApiData] = useState([]);
  const [isDark, setIsDark] = useState(false);

  useEffect(() => {
    callApi(api);
  }, []);

  async function callApi(url) {
    try {
      const res = await axios.get(url);
      setApiData(res.data);
    } catch (error) {
      console.error("Error calling API:", error);
    }
  }

  return (
    <div>
      <Header isDark={isDark} setIsDark={setIsDark} />

      <Routes>
        <Route
          path="/"
          element={
            <>
              <SearchFilter
                isDark={isDark}
                callApi={callApi}
                setApiData={setApiData}
                apiData={apiData}
              />

              <AllCards apiData={apiData} isDark={isDark} />
            </>
          }
        ></Route>

        <Route path="/flag/:id" element={<Cards isDark={isDark} />}></Route>
      </Routes>
    </div>
  );
};

export default App;
